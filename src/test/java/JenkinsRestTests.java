import com.offbytwo.jenkins.JenkinsServer;
import com.offbytwo.jenkins.JenkinsTriggerHelper;
import com.offbytwo.jenkins.model.Job;
import org.junit.jupiter.api.*;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class JenkinsRestTests {

    private JenkinsServer jenkinsServer;
    private final String jobName = "TestJob";

    @BeforeEach
    void setUp() throws URISyntaxException {

        jenkinsServer = new JenkinsServer(new URI("http://localhost:8080"));
    }

    @AfterEach
    void tearDown() {

        jenkinsServer.close();
    }

    @Order(1)
    @Test
    @DisplayName("Create Job Test")
    void createJobTest() throws IOException {

        String xml = new String(Files.readAllBytes(Paths.get("./src/test/resources/config.xml")));

        jenkinsServer.createJob(jobName, xml, true);

        assertEquals(jenkinsServer.getJob(jobName).getDisplayName(), jobName);

    }

    @Order(2)
    @Test
    @DisplayName("Get Job Test")
    void getJobTest() throws IOException {

        Map<String, Job> jobs = jenkinsServer.getJobs();

        assertTrue(jobs.values().stream().anyMatch(job -> job.getName().equals(jobName)));

    }

    @Order(3)
    @RepeatedTest(value = 5, name = RepeatedTest.LONG_DISPLAY_NAME)
    @DisplayName("Build Job Test")
    void buildJobTest() throws IOException, InterruptedException {

        Integer numberOfJobsBeforeBuild = jenkinsServer.getJob(jobName).getAllBuilds().size();

        JenkinsTriggerHelper helper = new JenkinsTriggerHelper(jenkinsServer);
        helper.triggerJobAndWaitUntilFinished(jobName, true);

        Integer numberOfJobsAfterBuild = jenkinsServer.getJob(jobName).getAllBuilds().size();

        assertEquals(1, numberOfJobsAfterBuild - numberOfJobsBeforeBuild);

    }

    @Order(4)
    @Test
    @DisplayName("Disable Job Test")
    void disableJobTest() throws IOException {

        jenkinsServer.disableJob(jobName, true);

        Boolean jobAfterDisable = jenkinsServer.getJob(jobName).isBuildable();

        assertFalse(jobAfterDisable);
    }

    @Order(5)
    @Test
    @DisplayName("Enable Job Test")
    void enableJobTest() throws IOException {

        jenkinsServer.enableJob(jobName, true);

        Boolean jobAfterEnable = jenkinsServer.getJob(jobName).isBuildable();

        assertTrue(jobAfterEnable);

    }

    @Order(6)
    @Test
    @DisplayName("Build Crumb False Job Test")
    void buildCrumbFalseJobTest() {

        JenkinsTriggerHelper helper = new JenkinsTriggerHelper(jenkinsServer);

        assertThrows(org.apache.http.client.HttpResponseException.class, () -> {

            helper.triggerJobAndWaitUntilFinished(jobName);

        });
    }

    @Order(7)
    @Test
    @DisplayName("Rename Job Test")
    void renameJobTest() throws IOException {

        String newJobName = "NewTestJob";

        jenkinsServer.renameJob(jobName, newJobName, true);
        Map<String, Job> jobs = jenkinsServer.getJobs();

        assertTrue(jobs.values().stream().anyMatch(job -> job.getName().equals(newJobName)));
    }

    @Order(8)
    @Test
    @DisplayName("Rename to old name Job Test")
    void renameOldNameJobTest() throws IOException {

        String newJobName = "NewTestJob";

        jenkinsServer.renameJob(newJobName, jobName, true);
        Map<String, Job> jobs = jenkinsServer.getJobs();

        assertTrue(jobs.values().stream().anyMatch(job -> job.getName().equals(jobName)));
    }

    @Order(9)
    @Test
    @DisplayName("Update Job Test")
    void updateJobTest() throws IOException {

        String jobXmlBeforeUpdate = jenkinsServer.getJobXml(jobName);

        String newXml = jobXmlBeforeUpdate.replace("<triggers/>",
                "<triggers><hudson.triggers.TimerTrigger>" +
                        "<spec>H 0 * * *</spec></hudson.triggers.TimerTrigger></triggers>");

        jenkinsServer.updateJob(jobName, newXml, true);
        String jobXmlAfterUpdate = jenkinsServer.getJobXml(jobName);

        assertNotEquals(jobXmlBeforeUpdate, jobXmlAfterUpdate);
    }

    @Order(10)
    @Test
    @DisplayName("Delete Job Test")
    void deleteJobTest() throws IOException {

        jenkinsServer.deleteJob(jobName, true);
        Map<String, Job> jobs = jenkinsServer.getJobs();

        assertTrue(jobs.values().stream().noneMatch(job -> job.getName().equals(jobName)));
    }

}
