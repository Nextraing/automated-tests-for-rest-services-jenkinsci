# Automated tests for REST services "Jenkins CI"

This repository contains tests among which are create, build, update and delete job.

### Tools

- [x] Java
- [x] Maven
- [x] JUnit 5
- [x] Allure

### Running the tests

`mvn clean test allure:report`

### Test results report

After running the tests you can find the report here:

`{your_project_package}/target/site/allure-maven-plugin/index.html`